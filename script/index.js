/* 
Створіть клас Phone, який містить змінні number, model і weight.
Створіть три екземпляри цього класу.
Виведіть на консоль значення їх змінних.
Додати в клас Phone методи: receiveCall, має один параметр - ім'я. Виводить на консоль повідомлення "Телефонує {name}". Метод getNumber повертає номер телефону. Викликати ці методи кожного з об'єктів.
*/

function Phone(number, model, weight) {
    this.number = number;
    this.model = model;
    this.weight = weight;

}

Phone.prototype.receiveCall = function (name) {
    console.log(`Телефонує ${name}`);
}
Phone.prototype.getNumber = function () {
    return console.log(`Номер телефону: ${this.number}`);
}

let phone1 = new Phone(123456789, 'nokia', 120);
let phone2 = new Phone(987654321, 'samsung', 130);
let phone3 = new Phone(852456137, 'iphone', 135);

for (let elem in phone1) {
    document.write(phone1[elem] + '<br>');
}
document.write('<hr>');

for (let elem in phone2) {
    document.write(phone2[elem] + '<br>');
}
document.write('<hr>');

for (let elem in phone3) {
    document.write(phone3[elem] + '<br>');
}
document.write('<hr color="red">');


/* 
Написати функцію filterBy(), яка прийматиме 2 аргументи. Перший аргумент - масив, який міститиме будь-які дані, другий аргумент - тип даних.
- Функція повинна повернути новий масив, який міститиме всі дані, які були передані в аргумент, за винятком тих, тип яких був переданий другим аргументом. Тобто якщо передати масив ['hello', 'world', 23, '23', null], і другим аргументом передати 'string', то функція поверне масив [23, null].
*/

const arr = [2,3,'a', 'b', null, true, '43', 43, false]

function filterBy(array, type) {
  let arr2 = [];
  for(let i = 0; i < array.length; i++) {
    if(typeof array[i] === type) {
        arr2.push(array[i]);
    }
  }
  return arr2;  
}
document.write('Результат фiльтру типу "string": ' + filterBy(arr, 'string')+'<br>');
document.write('Результат фiльтру типу "number": ' + filterBy(arr, 'number')+'<br>');
document.write('Результат фiльтру типу "boolean": ' + filterBy(arr, 'boolean')+'<br>');